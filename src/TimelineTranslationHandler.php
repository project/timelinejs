<?php

namespace Drupal\timelinejs;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for timeline.
 */
class TimelineTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
