<?php

namespace Drupal\timelinejs\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Timeline entities.
 *
 * @ingroup timelinejs
 */
class TimelineDeleteForm extends ContentEntityDeleteForm {


}
